var express = require('express');
var bodyParser = require("body-parser");
var User = require("./models/user").User; 
const app = express();

app.use("/public",express.static('public'));
app.use(bodyParser.json());//peticiones application /json
app.use(bodyParser.urlencoded({extended: true}));

app.set("view engine","jade");

const port = process.env.PORT || 3000;
const dateUp = Date.now();

app.get('/', (req, res) => {
/*  const today = new Date();
  res.json({
    date: today,
    up: `${(Date.now() - dateUp)/1000} seg.`,
  }); 
*/
  res.render("index");
});

app.get("/prueba",function(req,res) {
  User.find(function (err,doc) {
    res.send(doc);
    
  });  
});

//LOGIN user
app.post("/login",function(req,res) {
  console.log(req.body.correo);
  var user = new User({
      username:req.body.username,  
      correo:req.body.correo,      
      password:req.body.password,
      password_confirmation: req.body.password_confirmation});
  
  user.save(function(err) {
    if (err) {
      console.log(String(err));
    }
    res.send("GUARDADO");  
  });
  
});

app.listen(port, () => {
  console.log(`Server running on port: ${port}`);
  console.log('Press CTRL + C to quit');
});