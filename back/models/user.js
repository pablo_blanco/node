var mongoose = require("mongoose");
var Schema = mongoose.Schema;
mongoose.connect("mongodb://localhost");

var email_match = [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,"Coloca un email valido"];

var user_schema = new Schema({
    username: {type: String,required:"Usuario Obligatorio"},
    correo: {type: String, required: "El correo es obligatorio",match:email_match},
    password: {type:String,minlength:[8,"Contraseña mayor a 8 caracteres"]}
});

user_schema.virtual("password_confirmation").get(function () {
    return this.pass_confirmation;
}).set(function (password) {
    this.pass_confirmation = password;
});

var User = mongoose.model("User",user_schema);
module.exports.User = User;
